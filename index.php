<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Base de données</h1>
<?php
            $servername = 'localhost';
            $username = 'root';
            $password = 'root';
             //On essaie de se connecter
             try{
                $conn = new PDO("mysql:host=$servername;dbname=colyseum", $username, $password);
                //On définit le mode d'erreur de PDO sur Exception
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            /*On capture les exceptions si une exception est lancée et on affiche
             *les informations relatives à celle-ci*/
            catch(PDOException $e){
              echo "Erreur : " . $e->getMessage();
            }
            //Exercice 1 et 3
            // $query = $conn->query('SELECT * FROM clients LIMIT 0, 20');
            // $clients = $query->fetchAll();
            // print_r($clients);

            //Exercice 2
            // $query = $conn->query('SELECT * FROM shows');
            // $shows = $query->fetchAll();
            // print_r($shows);

            //Exercice 4
            // $query = $conn->query('SELECT firstName, card FROM clients');
            // $cards = $query->fetchall();
            // print_r($cards);

            //Exercice 5 et 7

// On récupère tout le contenu de la table clients
// $reponse = $conn->query('SELECT * FROM clients ORDER BY firstName ASC');

// // On affiche chaque entrée une à une
// while ($donnees = $reponse->fetch())
// {
// ?>
<!-- //     <p>
//     <strong>Clients</strong> : <br>
//     Nom :<?php echo $donnees['firstName']; ?><br/>
//     Prenom :<?php echo $donnees['lastName']; ?><br/>
//     Date de naissance :<?php echo $donnees['birthDate']; ?> <br>
//     Carte de fidelité :<?php echo $donnees['card'] ?> <br>
//     Numéro de carte :<?php echo $donnees['cardNumber'] ?>
//    </p> -->
// <?php
// }

// $reponse->closeCursor(); // Termine le traitement de la requête

// ?>
<?php
// On récupère tout le contenu de la table clients
$reponse = $conn->query('SELECT * FROM shows ORDER BY title ASC');

// On affiche chaque entrée une à une
while ($donnees = $reponse->fetch())
{
?>
    <p>
    <strong>Spétacle</strong> : <br>
    Le titre <?php echo $donnees['title']; ?> avec <?php echo $donnees['performer']; ?><br/>
    Le <?php echo $donnees['date']; ?> à <?php echo $donnees['startTime'] ?> <br>
   </p>
<?php
}

$reponse->closeCursor(); // Termine le traitement de la requête

?>
   
</body>
</html>

